# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

You will need to get the Source Tree software for Windows from the official website. Once you have downloaded and installed the Source Tree software, you can skip the setup of repository in the start. 

You can just login with your bitbucket account and follow these below steps.

Open the software and setup, go to Tools -> Options as shown below:

![screenshot.11924.png](https://bitbucket.org/repo/8kKB75/images/151315197-screenshot.11924.png)

Then you can setup the settings in General Tab, set up Full Name and Email Address for now as below:

![screenshot.11925.png](https://bitbucket.org/repo/8kKB75/images/12137722-screenshot.11925.png)

After setting up General settings, you can move to the Tab named "Authentication" and can add / update / delete your user credentials as shown below:

![screenshot.11926.png](https://bitbucket.org/repo/8kKB75/images/103671443-screenshot.11926.png)


After all this is setup then follow as below by clicking on the "Clone / New" button as shown:

![screenshot.11922.png](https://bitbucket.org/repo/8kKB75/images/3225629563-screenshot.11922.png)

The new window will open. Under the "Clone Repository" Tab, you will have to provide Source Path / Url, Destination Path. And in "Advance Options" collapsible provide the "Release-1" as Checkout Branch. You can provide a Bookmark name if desired. After making these changes, you can click "Clone" as shown below:

![screenshot.11923.png](https://bitbucket.org/repo/8kKB75/images/2846894241-screenshot.11923.png)

Now the cloning process will start and it may take few minutes depending upon your internet connection. After completion, you can close the window and switch back to main window. 

In main window, there will be the Newly added Repository name (Bookmark Name) visible. You can continue by exploring the cloned repository. 

You will find the new Project Structure with the folder name "Shipsall" at the root of the destination folder that was provided by you before cloning the repository as shown below:

![screenshot.11927.png](https://bitbucket.org/repo/8kKB75/images/3921948948-screenshot.11927.png)

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact