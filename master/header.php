<!-- 
Project Name : Ouiship
Developer : Prashant Gorvadia
header file includes all meta/title/javascripts/css/jquery and javascript related code and 
-->
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Oui! Ship | We Ship -Yes!</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Css File include here : Start -->
<link rel="shortcut icon" href="img/favicon.ico">
<link href="<?php echo SITE_URL; ?>css/general.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_URL; ?>css/jquery.bxslider.css" rel="stylesheet" />
<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link id="bs-css" href="<?php echo SITE_URL; ?>css/bootstrap-cerulean.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_URL; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_URL; ?>css/demo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo SITE_URL; ?>css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo SITE_URL; ?>css/animate.min.css">
<link rel="stylesheet" href="css/liquid-slider.css">
<link rel="stylesheet" href="<?php echo SITE_URL; ?>css/bootstrap-datetimepicker.css" />
<link href="<?php echo SITE_URL; ?>css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- Css File include here : End -->
<!-- jQuery File include here: Start -->
<script type="text/javascript" src="<?php echo JS_PATH; ?>jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH; ?>jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH; ?>accordion.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH; ?>jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH; ?>moment.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH; ?>bootstrap-switch.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH; ?>jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH; ?>bootstrap-datetimepicker.js"></script>
<!-- jQuery File include here: End -->
<!-- jquery dateandtime picker end -->
<script type="text/javascript">
	$(function(){
	$('a[href="#"]').on('click', function(e){
	e.preventDefault();
	});
	
	$('#menu > li').on('mouseover', function(e){
	$(this).find("ul:first").show();
	$(this).find('> a').addClass('active');
	}).on('mouseout', function(e){
	$(this).find("ul:first").hide();
	$(this).find('> a').removeClass('active');
	});
	
	$('#menu li li').on('mouseover',function(e){
	if($(this).has('ul').length) {
		$(this).parent().addClass('expanded');
	}
	$('ul:first',this).parent().find('> a').addClass('active');
	$('ul:first',this).show();
	}).on('mouseout',function(e){
	$(this).parent().removeClass('expanded');
	$('ul:first',this).parent().find('> a').removeClass('active');
	$('ul:first', this).hide();
	});
	});

	var date = new Date();
	date.setDate(date.getDate()-1);
	var nowDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);			 $(function() {
	$('#datetimepicker1').datetimepicker({daysOfWeekDisabled: [0,6],disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 18, 19, 20, 21, 22, 23, 24],minDate: today});
	
	
	});
	
	$(function() {
	$('#datetimepicker2').datetimepicker({daysOfWeekDisabled: [0,6],disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 18, 19, 20, 21, 22, 23, 24],minDate: today,useCurrent: false});
	});
	
	
	$(window).load(function () {
	var $content = $(".content").show();
	$(".toggle").on("click", function(e){
	$(this).toggleClass("expanded");
	$content.slideToggle();
	});
	
	
	$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
	if(state == true)
	$("#title_toggle_text").show();
	else
	$("#title_toggle_text").hide();
	
	});
	$("[name='my-checkbox']").bootstrapSwitch();
	$('[data-toggle="popover"]').popover({ html : true });   
	
	$( '.dropdown' ).hover(
	function(){
	$(this).children('.sub-menu').slideDown(200);
	},
	function(){
	$(this).children('.sub-menu').slideUp(200);
	}
	);
	
	
	$('.bxslider').bxSlider();
	// Page is fully loaded .. time to fade out your div with a timer ..
	$('#overlay').fadeOut(5000);
	$('#shiptabex').dataTable(
	{
	"order": [[ 3, "asc" ]]
	} 
	);
	});
	
	(function( $ ) {
	$.widget( "custom.combobox", {
	_create: function() {
	this.wrapper = $( "<span>" )
	.addClass( "custom-combobox" )
	.insertAfter( this.element );
	
	this.element.hide();
	this._createAutocomplete();
	this._createShowAllButton();
	},
	
	_createAutocomplete: function() {
	var selected = this.element.children( ":selected" ),
	value = selected.val() ? selected.text() : "";
	
	this.input = $( "<input>" )
	.appendTo( this.wrapper )
	.val( value )
	.attr( "title", "" )
	.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
	.autocomplete({
	delay: 0,
	minLength: 0,
	source: $.proxy( this, "_source" )
	})
	.tooltip({
	tooltipClass: "ui-state-highlight"
	});
	
	this._on( this.input, {
	autocompleteselect: function( event, ui ) {
	ui.item.option.selected = true;
	this._trigger( "select", event, {
	item: ui.item.option
	});
	},
	
	autocompletechange: "_removeIfInvalid"
	});
	},
	
	_createShowAllButton: function() {
	var input = this.input,
	wasOpen = false;
	
	$( "<a>" )
	.attr( "tabIndex", -1 )
	.attr( "title", "Show All Items" )
	.tooltip()
	.appendTo( this.wrapper )
	.button({
	icons: {
	primary: "ui-icon-triangle-1-s"
	},
	text: false
	})
	.removeClass( "ui-corner-all" )
	.addClass( "custom-combobox-toggle ui-corner-right" )
	.mousedown(function() {
	wasOpen = input.autocomplete( "widget" ).is( ":visible" );
	})
	.click(function() {
	input.focus();
	
	// Close if already visible
	if ( wasOpen ) {
	return;
	}
	
	// Pass empty string as value to search for, displaying all results
	input.autocomplete( "search", "" );
	});
	},
	
	_source: function( request, response ) {
	var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
	response( this.element.children( "option" ).map(function() {
	var text = $( this ).text();
	if ( this.value && ( !request.term || matcher.test(text) ) )
	return {
	label: text,
	value: text,
	option: this
	};
	}) );
	},
	
	_removeIfInvalid: function( event, ui ) {
	
	// Selected an item, nothing to do
	if ( ui.item ) {
	return;
	}
	
	// Search for a match (case-insensitive)
	var value = this.input.val(),
	valueLowerCase = value.toLowerCase(),
	valid = false;
	this.element.children( "option" ).each(function() {
	if ( $( this ).text().toLowerCase() === valueLowerCase ) {
	this.selected = valid = true;
	return false;
	}
	});
	
	// Found a match, nothing to do
	if ( valid ) {
	return;
	}
	
	// Remove invalid value
	this.input
	.val( "" )
	.attr( "title", value + " didn't match any item" )
	.tooltip( "open" );
	this.element.val( "" );
	this._delay(function() {
	this.input.tooltip( "close" ).attr( "title", "" );
	}, 2500 );
	this.input.autocomplete( "instance" ).term = "";
	},
	
	_destroy: function() {
	this.wrapper.remove();
	this.element.show();
	}
	});
	})( jQuery );
	
	$(function() {
	$( "#combobox" ).combobox();
	$( "#toggle" ).click(function() {
	$( "#combobox" ).toggle();
	});
	});
	$(function() {
	$( "#combobox2" ).combobox();
	$( "#toggle" ).click(function() {
	$( "#combobox2" ).toggle();
	});
	});


</script>
<!-- data table plugin -->
<!-- library for cookie management -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>
<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
</head>	
