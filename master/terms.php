<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//Terms page include ouiship terms and condition
include('config.php');
include('header.php');
?>
<body>
<?php include('header-menu.php'); ?>

	<div class="container">
    <div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
					
					<div class="about_box">
					<div class="about_heading">
						<h3>Terms and Conditions</h3>
					</div>
			
		
     <p>Terms and Conditions for Using <b>OUI! SHIP</b> INCORPORATED Website</p>
<p> <b>OUI! SHIP</b> INCORPORATED(Nevada) makes its Web Site available, subject to these Terms of Service ("Terms"), and by using this WebSite you indicate your agreement to be bound by these Terms. </p>

<p> 
<b>1.	Acceptance of Terms</b><br>
YOU MUST READ THESE TERMS OF USE ("TERMS") CAREFULLY. <b>OUI! SHIP</b> INCORPORATEDPROVIDES THIS WEB SITE TOYOU SUBJECT TO THESE TERMS. THESE TERMS ARE ENTERED INTO BY AND BETWEEN <b>OUI! SHIP</b> INCORPORATEDAND YOU,AND YOU ACCEPT THEM BY (a) USING THE WEB SITE IN ANY MANNER OR (b) ACKNOWLEDGING YOUR AGREEMENTTO THE TERMS BY CREATING A USER ACCOUNT TO ACCESS THE WEBSITE OR BY USING ANY <b>OUI! SHIP</b> INCORPORATEDTOOLS.</p>

<p  style="text-align:center"><b>IF YOU DO NOT AGREE TO ALL OF THESE TERMS, DO NOT USE THIS WEB SITE.</b></p>

<p>
<b>2.	Acceptable Use & License</b><br>
In consideration of your use of our Web Site, you agree to maintain the security of your password and identification. You are entirely responsible for all content that you upload, post or otherwise transmit via the Web Site. <b>OUI! SHIP</b> INCORPORATED grants you a limited, personal, nontransferable, non-sub licensable, revocable license to access and use this Site only as expressly permitted in this Agreement. Except for this limited license, we do not grant you any other rights or license with respect to this Site; any rights or license not expressly granted here in are reserved. The content and information on this Site (including, without limitation, price and availability of shipping services), as well as the infrastructure used to provide such content and information, is proprietary to <b>OUI! SHIP</b> INCORPORATED and/or it's Shipping Service Providers. Accordingly, as a condition of using this Site, you agree not to use this Site or its contents or information for any commercial or non-personal purpose (direct or indirect) or for any purpose that is unlawful or prohibited by this Agreement. 
	
While you may make limited copies of your shipping manifest(s) and related documents for shipping services purchased through this Site, you agree not to modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell or resell  any information, software, products, or services obtained from this Site. 
<br>
In addition, whether or not you have a commercial purpose, you agree not to:
<br>
<b>a.</b> access, monitor or copy any content or information of this Site using any robot, spider, scraper orother automated means or any manual process for any purpose without express written permissionof <b>OUI! SHIP</b> Incorporated<br>
<b>b.</b> violate the restrictions in any robot exclusion headers on this Site or bypass or circumvent othermeasures employed to prevent or limit access to this Site;<br>
<b>c.</b> take any action that imposes, or may impose, in the discretion of <b>OUI! SHIP</b> Incorporated, an unreasonable ordisproportionately large load on the <b>OUI! SHIP</b> INCORPORATED infrastructure;<br>
<b>d.</b> deeplink to any portion of this Site (including, without limitation, the purchase path for any shippingservices) for any purpose.<br>
<b>e.</b> manipulate identifiers, including by forging headers, in order to disguise the origin of any posting that you deliver;<br>
<b>f.</b> use this Site in any manner which could damage, disable, overburden, or impair or otherwiseinterfere with the use of this Site or other users' computer equipment, or cause damage, disruption or limit the functioning of any software, hardware, or telecommunications equipment;<br>
<b>g.</b> attempt to gain unauthorized access to this Site, any related website, other accounts, computersystem, or networks connected to this Site, through hacking, password mining, or any other means;<br>
<b>h.</b> obtain or attempt to obtain any materials or information through any means not intentionally madeavailable through this Site, including harvesting or otherwise collecting information about others suchas email addresses.<br>
<b>i.</b>	upload, post or otherwise transmit via the Web Site content that:<br> 
<b>(a)</b> is inaccurate, harmful, obscene, pornographic, defamatory, racist, violent, offensive, harassing, or otherwise objectionable to <b>OUI! SHIP</b> INCORPORATEDor other users of the Web Site; <br>
<b>(b)</b> includes unauthorized disclosure of personal information; <br>
<b>(c)</b> violates or infringes any person's rights, including without limitation intellectual property rights; <br>
<b>(d)</b> contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; or <br>
<b>(e)</b> contains commercial solicitations, advertisements, or any commercial links. <b>OUI! SHIP</b> INCORPORATEDreserves the right to edit or remove content that violates these Terms.<br>

You may only use this Site to make legitimate shipping inquiries, purchases or requests to purchase the products or services offered (each, a "Request") and shall not use this Site to make any speculative, false or fraudulent Requests or any Requests in anticipation of demand. Further, you agree that any discrepancies in entering the actual package weight, which result in additional charges from the Carrier in order to compensate for inaccurate package weight, will subsequently be charged to the same account or payment service from which the original shipping label was purchased within a reasonable period of time.<br>  

You represent that you are of sufficient legal age to create binding legal obligations for any liability you may incur as a result of your use of this Site. You agree to provide correct and true information in connection with your use of this Site and you agree to promptly update your account information (if applicable) in order to keep it current, complete and accurate. It is a violation of law to place a Request in a false name or with an invalid method of payment. Please be aware that even if you do not give us your real name, your web browser transmits a unique Internet address to us that can be used by law enforcement officials to identify you. Fraudulent users will be prosecuted to the fullest extent of the law.<br>
<b>OUI! SHIP</b> INCORPORATED reserves the right to cancel any Shipment or any other transaction that it reasonably believes to have been fraudulently made including, by unauthorized use of a credit or debitcard.
</p>

<p>
<b>3.	Accounts, Security, Passwords</b><br>
You may register to utilize this Site by completing the specified registration process and providing us with current, complete, and accurate information as requested by the online registration form. It is your responsibility to maintain the completeness and accuracy of your registration data, and any loss caused by your failure to do so is your responsibility. As part of the registration process, you will be asked to choose a security question. It is entirely your responsibility to maintain the confidentiality of your security question and your account. You agree to notify <b>OUI! SHIP</b> INCORPORATED immediately of any unauthorized use of your account. <b>OUI! SHIP</b> INCORPORATED is not liable for any loss that you may incur as a result of someone else using your account, either with or without your knowledge.
</p>

<p>
<b>4.	Advertisements and Promotion</b><br>
<b>OUI! SHIP</b> INCORPORATED may place advertisements and promotional materials from third parties on the Web Site forinformational purposes only. Your correspondence or business dealings with, or participation in promotions ofadvertisers found on or through the Web Site, including without limitation payment, warranties or representationsassociated with such dealings, are solely between you and such advertiser. <b>OUI! SHIP</b> INCORPORATED is not responsible orliable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presenceof such advertisers on the Web Site. <b>OUI! SHIP</b> Incorporated's inclusion of third party links is solely for your convenienceand does not constitute endorsement by <b>OUI! SHIP</b> Incorporated.
</p>

<p>
<b>5.	Links to other Internet Sites</b><br>
You will find many links to Internet sites and resources on <b>OUI! SHIP</b> INCORPORATED Web Site. You acknowledge and agreethat any such links are provided as a convenience, and that <b>OUI! SHIP</b> INCORPORATED is not responsible for the availabilityof such external sites or resources, and does not endorse and is not responsible or liable for any content,advertising, products, or other materials on or available from such sites or resources. <b>OUI! SHIP</b> INCORPORATED will not beresponsible or liable, directly or indirectly, for any actual or alleged damage or loss caused by or in connection withuse of or reliance on any such content, goods or services available on or through any such site or resource.
</p>

<p>
<b>6.	Intellectual Property Rights</b><br>
Except as expressly authorized by <b>OUI! SHIP</b> INCORPORATED or by content providers, you agree not to reproduce, modify,post links, rent, lease, loan, sell, distribute, mirror, frame, republish, download, transmit, or create derivative worksof the content available on or through the Web Site, in whole or in part, by any means. All content of the<b>OUI! SHIP</b> INCORPORATED Website is Copyright © 2015
<b>OUI! SHIP</b> INCORPORATED , 1546 Northwest 56thStreet, Seattle, Washington 98107 U.S.A. All rights reserved.

<b>OUI! SHIP</b> INCORPORATED respects intellectual property rights, and as a user of <b>OUI! SHIP</b> INCORPORATED Web Site you areexpected to do the same. If you believe that your work has been copied in a way that constitutes copyrightinfringement on our Web Site, please contact Curt Thiel (copyright agent) and provide the following information asrequired by the Online Copyright Infringement Liability Limitation Act of the Digital Millennium Copyright Act, 17
U.S.C. 512:
a. A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right thatis allegedly infringed;
b. Identification of the copyright work claimed to have been infringed, or, if multiple copyrighted works at a singleonline site are covered by a single notification, a representative list of such works at that site;
c. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and thatis to be removed or access to which is to be disabled, and information reasonably sufficient to permit us tolocate the material;
d. Information reasonably sufficient to permit us to contact the complaining party;
e. A statement that the complaining party has a good-faith belief that use of the material in the mannercomplained of is not authorized by the copyright owner, its agent, or the law; and 
f. A statement that theinformation in the notification is accurate, and under penalty of perjury, that the complaining party isauthorized to act on behalf of the owner of an exclusive right that is allegedly infringed.

Except as expressly provided in these Terms, nothing in these Terms shall be construed as conferring any license tointellectual property rights, whether by estoppel, implication, or otherwise.
You agree to assume all risk in using services made available to you on <b>OUI! SHIP</b> INCORPORATED 's Web Site, in downloading any content, and from any use thereof you may choose to make. You must take precautions (e.g.,virus scans, etc.) to ensure that whatever content you download does not contain viruses, worms or other items of adestructive nature. <b>OUI! SHIP</b> INCORPORATED provides no assurances that the content does not contain any viruses,problems or errors, nor that any such issues will be resolved.
</p>
<p>
<b>7.	Trademark Notice</b><br>
"<b>OUI! SHIP</b> INCORPORATED " as well as additional marks not mentioned here, are registered service marks of <b>OUI! SHIP</b> INCORPORATED . Other product and company names identified on this Site may be the name, trademark, trade name, service mark, logo, symbol or other proprietary designation of <b>OUI! SHIP</b> INCORPORATED or a third-party.
The use on this Site of any name, trade name, trademark, service mark, logo, symbol or other proprietary designation or marking of or belonging to any third-party, and the availability of specific goods or services from such third-party through this Site, should not be construed as an endorsement or sponsorship of this Site by any such third-party, or the participation by such third-party in the offering of goods, services or information through this Site.
</p>
<p>
<b>8. 	No Agency Relationship</b><br>
<b>OUI! SHIP</b> INCORPORATED does not agree to act as your agent or fiduciary in providing services through the Site.
</p>

<p>
<b>9.	Minors</b>
Persons under the age of 18 are not eligible to purchase, cancel or modify any services available through <b>OUI! SHIP</b> INCORPORATED .
</p>

<p>
<b>10.	Indemnification</b><br>
You agree to indemnify and hold <b>OUI! SHIP</b> INCORPORATED and its officers, directors, affiliates, subsidiaries, partners,agents, licensors, licensees, suppliers, employees and representatives harmless from any claim or demand,including reasonable attorneys' fees, made by any third party due to or arising out of your content, your use of orconnection to the Web Site, your violation of the Terms, or your violation of any rights of another.
</p>

<p>
<b>11.	Notices, Modification and Termination of Services; Amendment of Terms</b><br>
<b>OUI! SHIP</b> INCORPORATED may provide notice to you by e-mail, regular mail, or posting notices or links to notices on theWeb Site. <b>OUI! SHIP</b> INCORPORATED reserves the right at any time to modify, suspend or terminate the Web Site (or anypart thereof), or your use of or access to it, with or without notice. <b>OUI! SHIP</b> INCORPORATED may also delete or bar accessto or use of all related information and files. <b>OUI! SHIP</b> INCORPORATED will not be liable to you or any third-party for anymodification, suspension, or termination of the Web Site, or loss of related information. <b>OUI! SHIP</b> INCORPORATED mayamend these Terms at any time by posting the amended terms on this Web Site. Your use of the Web Site aftersuch posting will constitute consent to the modified Terms.
</p>


<p>
<b>12.	Response to Requests</b><br>
While <b>OUI! SHIP</b> INCORPORATED will use its good faith efforts to respond to Requests within the time periods indicated on this Site, no guarantee is made that the status of your Request will be made available to you within the stated processing time. Neither <b>OUI! SHIP</b> INCORPORATED nor its Shipping Service Providers are responsible for any errors or delays in responding to a Request including, without limitation, error or delays in responding to a Request caused by an incorrect email address or other data provided by you or other technical problems beyond their control.
</p>

<p>
<b>13.	International Use</b><br>
Accessing materials on this Site by certain persons in certain countries may not be lawful, and<b>OUI! SHIP</b> INCORPORATED makes no representation that materials on this Site are appropriate or available for use in locations outside of the United States. This website is strictly for use by US Citizens until further notice.
</p>


<p>
<b>14.	Disclaimer of Warranties</b><br>
YOUR USE OF OUR WEB SITE IS AT YOUR SOLE RISK UNLESS OTHERWISE EXPLICITLY STATED. <b>OUI! SHIP</b> INCORPORATED WEB SITE, INCLUDING ALL INFORMATION, SERVICES AND CONTENT IS PROVIDED ON AN "AS IS," "ASAVAILABLE," AND "WITH ALL FAULTS" BASIS. <b>OUI! SHIP</b> INCORPORATED DISCLAIMS ALL EXPRESS OR IMPLIEDCONDITIONS, REPRESENTATIONS, AND WARRANTIES OF ANY KIND, INCLUDING ANY IMPLIED WARRANTY ORCONDITION OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, ORNONINFRINGEMENT. <b>OUI! SHIP</b> INCORPORATED MAKES NO REPRESENTATIONS, WARRANTIES, CONDITIONS ORGUARANTEES AS TO THE USEFULNESS QUALITY, SUITABILITY, TRUTH, ACCURACY OR COMPLETENESS OF THE WEBSITE.

<b>OUI! SHIP</b> INCORPORATED MAKES NO WARRANTY OR REPRESENTATION THAT: (a) THE WEB SITE AVAILABILITY WILL BEUNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE; (b) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OFTHE WEB SITE WILL BE ACCURATE OR RELIABLE; (c) THE QUALITY OF ANY CONTENT, INFORMATION, OR OTHERMATERIAL OBTAINED FROM THE WEB SITE WILL MEET YOUR EXPECTATIONS OR REQUIREMENTS; OR (d) ANYERRORS IN THE WEB SITE WILL BE CORRECTED. NO INFORMATION, WHETHER WRITTEN OR ORAL OBTAINED BYYOU FROM THE WEB SITE SHALL CREATE ANY WARRANTY NOT EXPRESSLY PROVIDED IN THESE TERMS.
YOU ASSUME ALL RISK FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROMUSE OF, OR OBTAINING ANY CONTENT FROM, THE WEB SITE, INCLUDING WITHOUT LIMITATION ANY DAMAGESRESULTING FROM COMPUTER VIRUSES.
</p>

<p>
<b>15.	Limitation of Liability</b><br>
TO THE FULL EXTENT PERMITTED BY LAW, <b>OUI! SHIP</b> INCORPORATED IS NOT LIABLE FOR ANY DIRECT, INDIRECT,PUNITIVE, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES (INCLUDING, WITHOUTLIMITATION, LOSS OF BUSINESS, REVENUE, PROFITS, GOODWILL, USE, DATA OR ECONOMIC ADVANTAGE)ARISING OUT OF OR IN CONNECTION WITH THE WEB SITE, EVEN IF <b>OUI! SHIP</b> INCORPORATED HAS PREVIOUSLY BEENADVISED OF, OR REASONABLY COULD HAVE FORESEEN, THE POSSIBILITY OF SUCH DAMAGES, HOWEVER THEYARISE, WHETHER IN BREACH OF CONTRACT OR IN TORT (INCLUDING NEGLIGENCE), INCLUDING WITHOUTLIMITATION DAMAGES DUE TO: (a) THE USE OF OR THE INABILITY TO USE THE WEB SITE; (b) THE COST OFPROCUREMENT OF SUBSTITUTE SERVICES RESULTING FROM ANY DATA, INFORMATION OR SERVICES OBTAINED,OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO, THROUGH OR FROM THE WEB SITE; (c)STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE WEB SITE, INCLUDING WITHOUT LIMITATIONUNAUTHORIZED ACCESS TO OR ALTERATION OF TRANSMISSIONS OR DATA, MALICIOUS OR CRIMINAL BEHAVIOR,OR FALSE OR FRAUDULENT TRANSACTIONS; OR (d) CONTENT OR INFORMATION YOU MAY DOWNLOAD, USE,MODIFY OR DISTRIBUTE.
TO THE EXTENT THAT ANY JURISDICTION DOES NOT ALLOW THE EXCLUSION OR LIMITATION OF DIRECT,INCIDENTAL OR CONSEQUENTIAL DAMAGES, PORTIONS OF THE ABOVE LIMITATION OR EXCLUSION MAY NOTAPPLY.
</p>

<p>
<b>16.	Privacy Policy</b><br>
You agree to comply with all applicable laws and regulations, and the terms of The <b>OUI! SHIP</b> INCORPORATED 's PrivacyPolicy, with respect to any access, use and/or submission by you of any personal information in connection with thisWeb Site.

Collection and Use of Personal Information: <b>OUI! SHIP</b> INCORPORATED minimizes the collection of information thatpersonally identifies you or allows you to be contacted ("Personal Information"). In order to protect your privacy,<b>OUI! SHIP</b> INCORPORATED does not share your Personal Information with any third parties except to fulfill legal andregulatory obligations.

Security of Personal Information: To prevent unauthorized access, maintain data accuracy, and ensure theappropriate use of information, <b>OUI! SHIP</b> INCORPORATED has put into place appropriate physical, electronic, andmanagerial procedures to safeguard and secure all Personal Information that <b>OUI! SHIP</b> INCORPORATED collects online. <b>OUI! SHIP</b> INCORPORATED has procedures that limit employee access to your Personal Information to those employees with abusiness reason to know. <b>OUI! SHIP</b> INCORPORATED educates its employees about the importance of confidentiality andcustomer privacy through standard operating procedures, training programs, and internal policies. <b>OUI! SHIP</b> INCORPORATED will take appropriate disciplinary measures to enforce employee privacy responsibilities. If <b>OUI! SHIP</b> INCORPORATED hires vendors who may have access to your Personal Information, <b>OUI! SHIP</b> INCORPORATED will requirethem to conform to <b>OUI! SHIP</b> INCORPORATED 's privacy standards.
</p>

<p>
<b>17.	General Provisions</b><br>
These above Terms constitute the entire agreement between you and <b>OUI! SHIP</b> INCORPORATED relating to their subject matter,and cancel and supersede any prior versions of the Terms. No modification to the Terms will be binding, unless inwriting and signed or posted by an authorized <b>OUI! SHIP</b> INCORPORATED representative. You must not assign or otherwisetransfer the Terms or any right granted hereunder. You also may be subject to additional terms and conditions thatmay apply when you use <b>OUI! SHIP</b> INCORPORATED 's products or services or those of a third party.Washington State law and controlling U.S. federal law govern these Terms, any action related to the Terms, and/or your use of the Web Site. No choice of law rules of any jurisdiction will apply to any dispute under the Terms. 

Youand <b>OUI! SHIP</b> INCORPORATED agree to submit to the personal and exclusive jurisdiction of the courts located within thecounty of King, Washington, U.S.A.<b>OUI! SHIP</b> INCORPORATED expressly reserves and retains its rights under applicablelaws, including without limitation Title IV of the RCW.Rights and obligations under the Terms which by their nature should survive will remain in full effect aftertermination or expiration of the Terms, including without limitation Sections 10, 11, 14, 15 and 17.Any express waiver or failure to exercise promptly any right under the Terms will not create a continuing waiver orany expectation of non-enforcement. If any provision of the Terms is held invalid by any law or regulation of anygovernment, or by any court or arbitrator, you agree that such provision will be replaced with a new provision thataccomplishes the original purpose, and the other provisions of the Terms will remain in full force and effect.

You expressly understand and agree that any dispute, claim or controversy between you and <b>OUI! SHIP</b> INCORPORATED arising out of or in connection with the Terms or your use of the Web Site: (a) must be brought no later than one(1) year from the date the alleged claim occurred or arose; and (b) unless otherwise specified herein, shall beconclusively settled by binding arbitration. Such arbitration will take place before one (1) arbitrator in the County ofKing, and City of Seattle, and will be conducted in accordance with Title VII of the RCW. Unless the subject matter ofthe dispute is otherwise pre-empted by federal law, the arbitrator will be bound to apply legal principles inaccordance with Washington law, but without regard to its conflict of laws provisions. By agreeing to this arbitrationclause, neither party waives any defenses or immunities available under Washington or other applicable laws. Eachparty shall be solely responsible for payment of its own pro rata share of any expenses and fees incurred during thecourse of arbitration. In no event will any arbitrator have the power or authority to award consequential damages,indirect or special damages, lost profits, loss of goodwill, or speculative damages. You and <b>OUI! SHIP</b> INCORPORATED agreeto abide by all decisions and awards rendered in such proceedings, which: (c) shall be final and conclusive; (d) maybe entered in any court having jurisdiction thereof as a basis for judgment and execution of any order andcollection. Unless otherwise compelled by applicable law, the parties will keep confidential the existence of the claim,controversy or dispute from third parties (other than the arbitrator), as well as any determination, decision, or rulingby the arbitrator. 
Disputes of Four thousand dollars ($4,000) or less will be handled in Small Claims Court in theCounty of King and City of Seattle under the terms and conditions specified in this paragraph.Except for your obligation under Sec. 10 "Indemnification," nothing in these Terms, either express or implied, shall give orbe construed to give any person, legal or natural, or any other entity, any legal or equitable right, remedy or claimunder these Terms, or under any of its covenants and provisions, each such covenant and provision being for thesole and exclusive benefit of the parties to these Terms. No action may be brought against either party by any thirdparty claiming to be a third party beneficiary to these Terms or to the actions contemplated by these Terms. Nothingin these Terms is intended to relieve or discharge any obligation or liability of any third party to any party to theseTerms, and nothing in these Terms will give, or be deemed to give, any third party any right of subrogation or actionover or against either party to these Terms.
</p>
    </div>
    </div>
			</div>
            </div>
    
<?php
include('footer.php'); ?>