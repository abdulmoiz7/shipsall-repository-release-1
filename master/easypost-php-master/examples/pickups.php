<?php

require_once("../lib/easypost.php");

\EasyPost\EasyPost::setApiKey('LDNaaanQTfoWnVXBIDd8pw');

$to_address = \EasyPost\Address::create(
  array("name"    => "Sawyer Bateman",
                           "street1" => "4043 Rhoda way",
                           "street2" => "",
                           "city"    => "concord",
                           "state"   => "CA",
                           "zip"     => "94518",
						   "phone"   => "(323) 855-0394"));
$from_address = \EasyPost\Address::create(
array("name"    => "Jon Calhoun",
                             "street1" => "2616 NE 72nd Street",
                             "street2" => "",
                             "city"    => "Seattle",
                             "state"   => "WA",
                             "zip"     => "98115",
							 "phone"   => "(415) 379-7678")
							 );
$parcel = \EasyPost\Parcel::create(
    array(
        "height" => 12,
        "length" => 12,
        "width" => 12,
        "weight" => 80
    )
);
$shipment = \EasyPost\Shipment::create(
    array(
        "to_address"   => $to_address,
        "from_address" => $from_address,
        "parcel"       => $parcel
    )
);
//$shipment->buy($shipment->lowest_rate(array('UPS','Fedex')));
$shipment->buy($shipment->lowest_rate(array('UPS'), array('Ground')));


//echo $shipment->id;
$pickup = \EasyPost\Pickup::create(
    array(
        "address" => $from_address,
        "shipment"=> $shipment,
        "reference" => $shipment->id,
        "max_datetime" => date("Y-m-d H:i:s"),
        "min_datetime" => date("Y-m-d H:i:s", strtotime('+1 day')),
        "is_account_address" => false,
        "instructions" => "Will be next to garage"
    )
);

$pickup->buy(array('carrier'=>'UPS', 'service' => 'Future-day Pickup'));
echo "Confirmation: " . $pickup->confirmation . "\n";

?>