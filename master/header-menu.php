<!-- This page include code for header menu code -->
<header class="header_section">
<div class="top_header">
	<div class="container">
		<div class="row">
			<div class="login_mail">
				<ul>
					<li><a href="#"><i class="fa fa-phone"></i>Call Us: 206-486-8686</a></li>
					<li><a href="#"><i class="fa fa-envelope"></i>Email Us: service@ouiship.com</a></li>
                    <?php if($_SESSION['userid']=='') { ?> <li><a href="<?php echo SITE_URL."loginsetupacct.php";?>"><i class="fa fa-lock"></i>Login/Setup Account</a></li><?php } else {?> 
                    <li><a href="<?php echo SITE_URL."logout.php";?>">Log Out</a></li>
<li><a href="<?php echo SITE_URL."loginsetupacct.php";?>">Setup Account</a></li>
					<li><?php echo "Welcome&nbsp;<span  style='text-transform:uppercase'><b>".getfirstname($_SESSION['userid'])."</b></span>"; ?></li>
					<li><?php echo "Philanthropy <b>".selectnonprofitorgname($_SESSION['userid'])."</b>"; ?></li>
					<?php } ?>
				</ul>
			</div>
            <!-- thise would be use for future purpose
			<div class="shiplogos">
					<a href="https://www.usps.com/" target="_blank"><img src="images/usps_icon.png" /></a>
					<a href="http://www.fedex.com/"  target="_blank"><img src="images/fedex_icon.png"/></a>
					<a href="http://www.dhl.com/en.html" target="_blank"><img src="images/dhl_icon.png" /></a>
					<a href="http://www.ups.com/" target="_blank"><img src="images/ups_icon.png" /></a>
				</div> -->
		</div>
	</div>
</div>
<div class="header_two">
	<div class="container">
		<div class="row">
			<div class="col-sm-2 col-md-2 col-lg-2">
				<div class="logo">
		 <a href="<?php echo SITE_URL; ?>"><img src="images/logo.png" /></a>
          </div>
			</div>
			<div class="col-sm-10 col-md-10 col-lg-10" id="site_nav_bar">
            
				<div class="header_nav">
                    <nav>
					<ul>
						<li><a href="<?php echo SITE_URL; ?>">home</a></li>
						<li><a href="<?php echo SITE_URL; ?>aboutus.php">about us</a></li>
						<li class="dropdown"><a href="#">services</a>
                          <ul class="sub-menu">
                            <li><a href="<?php echo SITE_URL; ?>ship_package.php?red=ship_package">Ship Package</a></li>
                            <li><a href="<?php echo SITE_URL; ?>compare_price.php">Compare Prices</a></li>
                            <li><a href="#">Customer Service</a></li>
                            <li><a href="<?php echo SITE_URL; ?>tracking_package.php">Track Package</a></li>
                          </ul>
                        </li>
						<li><a href="<?php echo SITE_URL; ?>philanthrophy.php">Philanthrophy</a></li>
						<li><a href="<?php echo SITE_URL; ?>newsroom.php">Newsroom</a></li>
						<li><a href="<?php echo SITE_URL; ?>contactus.php">contact us</a></li>
						<li>
							<div class="search_box"><a href="#"><i class="fa fa-search"></i></a></div>
						</li>
					</ul>
                    </nav>
				</div>
			</div>
		</div>
	</div>
</div>
</header>
<br><br><br><br>