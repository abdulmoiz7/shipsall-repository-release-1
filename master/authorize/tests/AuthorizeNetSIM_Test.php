<?php
include('../../config.php');

require '../autoload.php';
class AuthorizeNetSIM_Test
{

    public function testGenerateHash()
    {
        $_POST['x_amount'] = "4.12";
        $_POST['x_trans_id'] = "123";
        $message = new AuthorizeNetSIM("52QtkJ2m","test");
        $this->assertEquals("8FC33C32ABB3EDD8BBC4BE3E904CB47E",$message->generateHash());
    }

    public function testAmount()
    {
        $_POST['x_amount'] = "4.12";
        $_POST['x_response_code'] = "1";
        $message = new AuthorizeNetSIM("52QtkJ2m","test");
        $this->assertEquals("4.12",$message->amount);
        $this->assertTrue($message->approved);
    }

    public function testIsAuthNet()
    {
        $_POST['x_amount'] = "4.12";
        $_POST['x_trans_id'] = "123";
        $_POST['x_MD5_Hash'] = "8FC33C32ABB3EDD8BBC4BE3E904CB47E";
        $message = new AuthorizeNetSIM("52QtkJ2m","test");
        $this->assertTrue($message->isAuthorizeNet());
        $_POST['x_amount'] = "4.12";
        $_POST['x_trans_id'] = "123";
        $_POST['x_MD5_Hash'] = "8FC33C32BB3EDD8BBC4BE3E904CB47E";
        $message = new AuthorizeNetSIM("52QtkJ2m","test");
        $this->assertFalse($message->isAuthorizeNet());
    }

     public function testIsError()
    {
        $_POST['x_amount'] = "4.12";
        $_POST['x_response_code'] = "3";
        $_POST['x_ship_to_state'] = "CA";
        $message = new AuthorizeNetSIM("52QtkJ2m","test");
        $this->assertEquals("3",$message->response_code);
        $this->assertTrue($message->error);
        $this->assertFalse($message->approved);
        $this->assertEquals("CA",$message->ship_to_state);
    }

}
// Receive the posted amount and format it as a dollar amount without the currency symbol 
$amount        = 9.95;

// Generate a random sequence number (required by SIM API) 
$sequence    = rand(1, 1000); 

// Generate a timestamp 
$timestamp    = time (); 

// The following lines generate the SIM fingerprint.  PHP versions 5.1.2 and 
// newer have the necessary hmac function built in.  For older versions, it 
// will try to use the mhash library. 
$login = '52QtkJ2m';
$key = '8x665aPMR2jNB34d';
if( phpversion() >= '5.1.2' ) 
{    $fingerprint = hash_hmac("md5", $login . "^" . $sequence . "^" . $timestamp . "^" . $amount . "^", $key); } 
else  
{ $fingerprint = bin2hex(mhash(MHASH_MD5, $login . "^" . $sequence . "^" . $timestamp . "^" . $amount . "^", $key)); } 
?>
<script type="text/javascript">
window.onload = function(){
  document.forms['SIM_AUTHORIZE'].submit()

}
</script>
<FORM METHOD=POST name="SIM_AUTHORIZE" ACTION="https://test.authorize.net/gateway/transact.dll">
<INPUT TYPE=HIDDEN NAME="x_login" VALUE="52QtkJ2m">
<INPUT TYPE=HIDDEN NAME="x_fp_hash" VALUE="<?php echo $fingerprint; ?>">
<INPUT TYPE=HIDDEN NAME="x_fp_sequence" VALUE="<?php echo $sequence ; ?>">
<INPUT TYPE=HIDDEN NAME="x_fp_timestamp" VALUE="<?php echo $timestamp; ?>">
<INPUT TYPE=HIDDEN NAME="x_version" VALUE="3.1">
<INPUT TYPE=HIDDEN NAME="x_type" VALUE="AUTH_CAPTURE">
<INPUT TYPE=HIDDEN NAME="x_show_form" VALUE="PAYMENT_FORM">
<INPUT TYPE=HIDDEN NAME="x_invoice_num" VALUE="ORDER-002450">
<INPUT TYPE=HIDDEN NAME="x_description" VALUE="Product or order description.">
<INPUT TYPE=HIDDEN NAME="x_cust_id" VALUE="Doe-John 001">
<INPUT TYPE=HIDDEN NAME="x_amount" VALUE="<?php echo $amount; ?>">
<INPUT TYPE=HIDDEN NAME="x_first_name" VALUE="<?php echo $_REQUEST['fname']; ?>">
<INPUT TYPE=HIDDEN NAME="x_last_name" VALUE="<?php echo $_REQUEST['lname']; ?>">
<INPUT TYPE=HIDDEN NAME="x_address" VALUE="<?php echo $_REQUEST['street1']; ?>">
<INPUT TYPE=HIDDEN NAME="x_city" VALUE="<?php echo $_REQUEST['city']; ?>">
<INPUT TYPE=HIDDEN NAME="x_state" VALUE="<?php echo $_REQUEST['state']; ?>">
<INPUT TYPE=HIDDEN NAME="x_zip" VALUE="<?php echo $_REQUEST['zip']; ?>">
<INPUT TYPE=HIDDEN NAME="x_phone" VALUE="<?php echo $_REQUEST['phone']; ?>">
<INPUT TYPE=HIDDEN NAME="x_email" VALUE="<?php echo "phgorvadia@gmail.com"; ?>">
<INPUT TYPE=HIDDEN NAME="x_receipt_link_method" VALUE="LINK">
<INPUT TYPE=HIDDEN NAME="x_receipt_link_text" VALUE="Click here to return to our home page">
<INPUT TYPE=HIDDEN NAME="x_receipt_link_URL" VALUE="http://www.mydomain.com">
</FORM>