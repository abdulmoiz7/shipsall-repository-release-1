<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//Contact us page include Contact us page static content 
include('config.php');
include('header.php');
?>
<body>
<?php include('header-menu.php'); ?>

	<div class="container" style="padding-left:17%;">
		<div class="row">
			<div class="col-sm-10 col-md-10 col-lg-10">
				<div class="about_box">
					<div class="about_heading">
						<h3>Contact US</h3>
					</div>
					<p>
                    <b>Address</b><br>
                    Oui! Ship Incorporated<br>
                    1546 Northwest 56th Street<br>
                    Seattle, WA 98107<br><br>
                    <b>Phone : </b>206-486-8686<br>
                    <b>Toll Free : </b>866-986-7189<br>
                    <b>Fax : </b>206-905-2495<br><br>
                      
                    <b>Customer Service</b><br>
                    service@ouiship.com<br><br>
                    
                    <b>Business Hours : </b> 9am – 5pm PST                  
                   
                    </p>
				</div>
			</div>
			
		</div>
	</div>
<?php
include('footer.php'); ?>